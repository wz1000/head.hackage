diff --git a/Data/Primitive/Types.hs b/Data/Primitive/Types.hs
index ecca954..fa2334e 100644
--- a/Data/Primitive/Types.hs
+++ b/Data/Primitive/Types.hs
@@ -213,7 +213,7 @@ instance Prim a => Storable (PrimStorable a) where
   pokeElemOff (Ptr addr#) (I# i#) (PrimStorable a) = primitive_ $ \s# ->
     writeOffAddr# addr# i# a s#
 
-#define derivePrim(ty, ctr, sz, align, idx_arr, rd_arr, wr_arr, set_arr, idx_addr, rd_addr, wr_addr, set_addr) \
+#define derivePrim(ty, ctr, sz, align, idx_arr, rd_arr, wr_arr, set_arr, idx_addr, rd_addr, wr_addr, set_addr, extend) \
 instance Prim (ty) where {                                      \
   sizeOf# _ = unI# sz                                           \
 ; alignment# _ = unI# align                                     \
@@ -225,7 +225,8 @@ instance Prim (ty) where {                                      \
     = let { i = fromIntegral (I# i#)                            \
           ; n = fromIntegral (I# n#)                            \
           } in                                                  \
-      case unsafeCoerce# (internal (set_arr arr# i n x#)) s# of \
+      case unsafeCoerce# (internal                              \
+             (set_arr arr# i n (extend x#))) s# of              \
         { (# s1#, _ #) -> s1# }                                 \
                                                                 \
 ; indexOffAddr# addr# i# = ctr (idx_addr addr# i#)              \
@@ -236,7 +237,8 @@ instance Prim (ty) where {                                      \
     = let { i = fromIntegral (I# i#)                            \
           ; n = fromIntegral (I# n#)                            \
           } in                                                  \
-      case unsafeCoerce# (internal (set_addr addr# i n x#)) s# of \
+      case unsafeCoerce# (internal                              \
+             (set_addr addr# i n (extend x#))) s# of            \
         { (# s1#, _ #) -> s1# }                                 \
 ; {-# INLINE sizeOf# #-}                                        \
 ; {-# INLINE alignment# #-}                                     \
@@ -250,6 +252,48 @@ instance Prim (ty) where {                                      \
 ; {-# INLINE setOffAddr# #-}                                    \
 }
 
+-- Unfortunately, one cannot write a levity-polymorphic `id` function, so we do
+-- the next-best thing of inlining `id`'s implementation
+#define idLev (\x -> x)
+
+#if MIN_VERSION_base(4,16,0)
+int8ToIntCompat# :: Int8# -> Int#
+int8ToIntCompat# = int8ToInt#
+
+int16ToIntCompat# :: Int16# -> Int#
+int16ToIntCompat# = int16ToInt#
+
+int32ToIntCompat# :: Int32# -> Int#
+int32ToIntCompat# = int32ToInt#
+
+word8ToWordCompat# :: Word8# -> Word#
+word8ToWordCompat# = word8ToWord#
+
+word16ToWordCompat# :: Word16# -> Word#
+word16ToWordCompat# = word16ToWord#
+
+word32ToWordCompat# :: Word32# -> Word#
+word32ToWordCompat# = word32ToWord#
+#else
+int8ToIntCompat# :: Int# -> Int#
+int8ToIntCompat# x = x
+
+int16ToIntCompat# :: Int# -> Int#
+int16ToIntCompat# x = x
+
+int32ToIntCompat# :: Int# -> Int#
+int32ToIntCompat# x = x
+
+word8ToWordCompat# :: Word# -> Word#
+word8ToWordCompat# x = x
+
+word16ToWordCompat# :: Word# -> Word#
+word16ToWordCompat# x = x
+
+word32ToWordCompat# :: Word# -> Word#
+word32ToWordCompat# x = x
+#endif
+
 #if __GLASGOW_HASKELL__ >= 710
 liberate# :: State# s -> State# r
 liberate# = unsafeCoerce#
@@ -269,52 +313,68 @@ unI# (I# n#) = n#
 
 derivePrim(Word, W#, sIZEOF_WORD, aLIGNMENT_WORD,
            indexWordArray#, readWordArray#, writeWordArray#, setWordArray#,
-           indexWordOffAddr#, readWordOffAddr#, writeWordOffAddr#, setWordOffAddr#)
+           indexWordOffAddr#, readWordOffAddr#, writeWordOffAddr#, setWordOffAddr#,
+           idLev)
 derivePrim(Word8, W8#, sIZEOF_WORD8, aLIGNMENT_WORD8,
            indexWord8Array#, readWord8Array#, writeWord8Array#, shimmedSetWord8Array#,
-           indexWord8OffAddr#, readWord8OffAddr#, writeWord8OffAddr#, setWord8OffAddr#)
+           indexWord8OffAddr#, readWord8OffAddr#, writeWord8OffAddr#, setWord8OffAddr#,
+           word8ToWordCompat#)
 derivePrim(Word16, W16#, sIZEOF_WORD16, aLIGNMENT_WORD16,
            indexWord16Array#, readWord16Array#, writeWord16Array#, setWord16Array#,
-           indexWord16OffAddr#, readWord16OffAddr#, writeWord16OffAddr#, setWord16OffAddr#)
+           indexWord16OffAddr#, readWord16OffAddr#, writeWord16OffAddr#, setWord16OffAddr#,
+           word16ToWordCompat#)
 derivePrim(Word32, W32#, sIZEOF_WORD32, aLIGNMENT_WORD32,
            indexWord32Array#, readWord32Array#, writeWord32Array#, setWord32Array#,
-           indexWord32OffAddr#, readWord32OffAddr#, writeWord32OffAddr#, setWord32OffAddr#)
+           indexWord32OffAddr#, readWord32OffAddr#, writeWord32OffAddr#, setWord32OffAddr#,
+           word32ToWordCompat#)
 derivePrim(Word64, W64#, sIZEOF_WORD64, aLIGNMENT_WORD64,
            indexWord64Array#, readWord64Array#, writeWord64Array#, setWord64Array#,
-           indexWord64OffAddr#, readWord64OffAddr#, writeWord64OffAddr#, setWord64OffAddr#)
+           indexWord64OffAddr#, readWord64OffAddr#, writeWord64OffAddr#, setWord64OffAddr#,
+           idLev)
 derivePrim(Int, I#, sIZEOF_INT, aLIGNMENT_INT,
            indexIntArray#, readIntArray#, writeIntArray#, setIntArray#,
-           indexIntOffAddr#, readIntOffAddr#, writeIntOffAddr#, setIntOffAddr#)
+           indexIntOffAddr#, readIntOffAddr#, writeIntOffAddr#, setIntOffAddr#,
+           idLev)
 derivePrim(Int8, I8#, sIZEOF_INT8, aLIGNMENT_INT8,
            indexInt8Array#, readInt8Array#, writeInt8Array#, shimmedSetInt8Array#,
-           indexInt8OffAddr#, readInt8OffAddr#, writeInt8OffAddr#, setInt8OffAddr#)
+           indexInt8OffAddr#, readInt8OffAddr#, writeInt8OffAddr#, setInt8OffAddr#,
+           int8ToIntCompat#)
 derivePrim(Int16, I16#, sIZEOF_INT16, aLIGNMENT_INT16,
            indexInt16Array#, readInt16Array#, writeInt16Array#, setInt16Array#,
-           indexInt16OffAddr#, readInt16OffAddr#, writeInt16OffAddr#, setInt16OffAddr#)
+           indexInt16OffAddr#, readInt16OffAddr#, writeInt16OffAddr#, setInt16OffAddr#,
+           int16ToIntCompat#)
 derivePrim(Int32, I32#, sIZEOF_INT32, aLIGNMENT_INT32,
            indexInt32Array#, readInt32Array#, writeInt32Array#, setInt32Array#,
-           indexInt32OffAddr#, readInt32OffAddr#, writeInt32OffAddr#, setInt32OffAddr#)
+           indexInt32OffAddr#, readInt32OffAddr#, writeInt32OffAddr#, setInt32OffAddr#,
+           int32ToIntCompat#)
 derivePrim(Int64, I64#, sIZEOF_INT64, aLIGNMENT_INT64,
            indexInt64Array#, readInt64Array#, writeInt64Array#, setInt64Array#,
-           indexInt64OffAddr#, readInt64OffAddr#, writeInt64OffAddr#, setInt64OffAddr#)
+           indexInt64OffAddr#, readInt64OffAddr#, writeInt64OffAddr#, setInt64OffAddr#,
+           idLev)
 derivePrim(Float, F#, sIZEOF_FLOAT, aLIGNMENT_FLOAT,
            indexFloatArray#, readFloatArray#, writeFloatArray#, setFloatArray#,
-           indexFloatOffAddr#, readFloatOffAddr#, writeFloatOffAddr#, setFloatOffAddr#)
+           indexFloatOffAddr#, readFloatOffAddr#, writeFloatOffAddr#, setFloatOffAddr#,
+           idLev)
 derivePrim(Double, D#, sIZEOF_DOUBLE, aLIGNMENT_DOUBLE,
            indexDoubleArray#, readDoubleArray#, writeDoubleArray#, setDoubleArray#,
-           indexDoubleOffAddr#, readDoubleOffAddr#, writeDoubleOffAddr#, setDoubleOffAddr#)
+           indexDoubleOffAddr#, readDoubleOffAddr#, writeDoubleOffAddr#, setDoubleOffAddr#,
+           idLev)
 derivePrim(Char, C#, sIZEOF_CHAR, aLIGNMENT_CHAR,
            indexWideCharArray#, readWideCharArray#, writeWideCharArray#, setWideCharArray#,
-           indexWideCharOffAddr#, readWideCharOffAddr#, writeWideCharOffAddr#, setWideCharOffAddr#)
+           indexWideCharOffAddr#, readWideCharOffAddr#, writeWideCharOffAddr#, setWideCharOffAddr#,
+           idLev)
 derivePrim(Ptr a, Ptr, sIZEOF_PTR, aLIGNMENT_PTR,
            indexAddrArray#, readAddrArray#, writeAddrArray#, setAddrArray#,
-           indexAddrOffAddr#, readAddrOffAddr#, writeAddrOffAddr#, setAddrOffAddr#)
+           indexAddrOffAddr#, readAddrOffAddr#, writeAddrOffAddr#, setAddrOffAddr#,
+           idLev)
 derivePrim(StablePtr a, StablePtr, sIZEOF_PTR, aLIGNMENT_PTR,
            indexStablePtrArray#, readStablePtrArray#, writeStablePtrArray#, setStablePtrArray#,
-           indexStablePtrOffAddr#, readStablePtrOffAddr#, writeStablePtrOffAddr#, setStablePtrOffAddr#)
+           indexStablePtrOffAddr#, readStablePtrOffAddr#, writeStablePtrOffAddr#, setStablePtrOffAddr#,
+           idLev)
 derivePrim(FunPtr a, FunPtr, sIZEOF_PTR, aLIGNMENT_PTR,
            indexAddrArray#, readAddrArray#, writeAddrArray#, setAddrArray#,
-           indexAddrOffAddr#, readAddrOffAddr#, writeAddrOffAddr#, setAddrOffAddr#)
+           indexAddrOffAddr#, readAddrOffAddr#, writeAddrOffAddr#, setAddrOffAddr#,
+           idLev)
 
 -- Prim instances for newtypes in Foreign.C.Types
 deriving instance Prim CChar
@@ -421,8 +481,8 @@ deriving instance Prim Fd
 
 -- | @since 0.7.1.0
 instance Prim WordPtr where
-  sizeOf# _ = sizeOf# (undefined :: Ptr ()) 
-  alignment# _ = alignment# (undefined :: Ptr ()) 
+  sizeOf# _ = sizeOf# (undefined :: Ptr ())
+  alignment# _ = alignment# (undefined :: Ptr ())
   indexByteArray# a i = ptrToWordPtr (indexByteArray# a i)
   readByteArray# a i s0 = case readByteArray# a i s0 of
     (# s1, p #) -> (# s1, ptrToWordPtr p #)
@@ -433,11 +493,11 @@ instance Prim WordPtr where
     (# s1, p #) -> (# s1, ptrToWordPtr p #)
   writeOffAddr# a i wp = writeOffAddr# a i (wordPtrToPtr wp)
   setOffAddr# a i n wp = setOffAddr# a i n (wordPtrToPtr wp)
-  
+
 -- | @since 0.7.1.0
 instance Prim IntPtr where
-  sizeOf# _ = sizeOf# (undefined :: Ptr ()) 
-  alignment# _ = alignment# (undefined :: Ptr ()) 
+  sizeOf# _ = sizeOf# (undefined :: Ptr ())
+  alignment# _ = alignment# (undefined :: Ptr ())
   indexByteArray# a i = ptrToIntPtr (indexByteArray# a i)
   readByteArray# a i s0 = case readByteArray# a i s0 of
     (# s1, p #) -> (# s1, ptrToIntPtr p #)
diff --git a/primitive.cabal b/primitive.cabal
index f8fe470..5eb23c3 100644
--- a/primitive.cabal
+++ b/primitive.cabal
@@ -1,6 +1,7 @@
 Cabal-Version: 2.2
 Name:           primitive
 Version:        0.7.1.0
+x-revision: 2
 License:        BSD-3-Clause
 License-File:   LICENSE
 
@@ -53,7 +54,7 @@ Library
         Data.Primitive.Internal.Compat
         Data.Primitive.Internal.Operations
 
-  Build-Depends: base >= 4.5 && < 4.15
+  Build-Depends: base >= 4.5 && < 4.16
                , deepseq >= 1.1 && < 1.5
                , transformers >= 0.2 && < 0.6
   if !impl(ghc >= 8.0)
@@ -82,7 +83,7 @@ test-suite test-qc
                , ghc-prim
                , primitive
                , quickcheck-classes-base >=0.6 && <0.7
-               , QuickCheck ^>= 2.13
+               , QuickCheck >= 2.13 && < 2.15
                , tasty ^>= 1.2
                , tasty-quickcheck
                , tagged
