diff --git a/Language/Haskell/TH/Desugar.hs b/Language/Haskell/TH/Desugar.hs
index 626a689..ba8f2c4 100644
--- a/Language/Haskell/TH/Desugar.hs
+++ b/Language/Haskell/TH/Desugar.hs
@@ -134,7 +134,7 @@ import Language.Haskell.TH.Syntax
 import Control.Monad
 import qualified Data.Foldable as F
 import Data.Function
-import Data.List
+import Data.List (deleteFirstsBy)
 import qualified Data.Map as M
 import qualified Data.Set as S
 import Prelude hiding ( exp )
@@ -163,9 +163,9 @@ instance Desugar Cxt DCxt where
   desugar = dsCxt
   sweeten = cxtToTH
 
-instance Desugar TyVarBndr DTyVarBndr where
+instance Desugar TyVarBndrUnit DTyVarBndr where
   desugar = dsTvb
-  sweeten = tvbToTH
+  sweeten = tvbToTHUnit
 
 instance Desugar [Dec] [DDec] where
   desugar = dsDecs
diff --git a/Language/Haskell/TH/Desugar/Core.hs b/Language/Haskell/TH/Desugar/Core.hs
index 21b1fbe..a5f06ad 100644
--- a/Language/Haskell/TH/Desugar/Core.hs
+++ b/Language/Haskell/TH/Desugar/Core.hs
@@ -85,8 +85,16 @@ dsExp (LamCaseE matches) = do
 dsExp (TupE exps) = dsTup tupleDataName exps
 dsExp (UnboxedTupE exps) = dsTup unboxedTupleDataName exps
 dsExp (CondE e1 e2 e3) =
-  dsExp (CaseE e1 [ Match (ConP 'True [])  (NormalB e2) []
-                  , Match (ConP 'False []) (NormalB e3) [] ])
+  dsExp (CaseE e1 [ Match (ConP 'True
+#if MIN_VERSION_template_haskell(2,18,0)
+                                 []
+#endif
+                                 [])  (NormalB e2) []
+                  , Match (ConP 'False
+#if MIN_VERSION_template_haskell(2,18,0)
+                                 []
+#endif
+                                 []) (NormalB e3) [] ])
 dsExp (MultiIfE guarded_exps) =
   let failure = DAppE (DVarE 'error) (DLitE (StringL "Non-exhaustive guards in multi-way if")) in
   dsGuards guarded_exps failure
@@ -105,7 +113,11 @@ dsExp (CaseE exp matches) = do
   matches' <- dsMatches scrutinee matches
   return $ DLetE [DValD (DVarP scrutinee) exp'] $
            DCaseE (DVarE scrutinee) matches'
-dsExp (DoE stmts) = dsDoStmts stmts
+dsExp (DoE
+#if MIN_VERSION_template_haskell(2,17,0)
+           _
+#endif
+           stmts) = dsDoStmts stmts
 dsExp (CompE stmts) = dsComp stmts
 dsExp (ArithSeqE (FromR exp)) = DAppE (DVarE 'enumFrom) <$> dsExp exp
 dsExp (ArithSeqE (FromThenR exp1 exp2)) =
@@ -509,7 +521,11 @@ dsParComp (q : rest) = do
   (rest_pat, rest_exp) <- dsParComp rest
   dsQ <- dsComp (q ++ [mk_tuple_stmt qv])
   let zipped = DAppE (DAppE (DVarE 'mzip) dsQ) rest_exp
-  return (ConP (tupleDataName 2) [mk_tuple_pat qv, rest_pat], zipped)
+  return (ConP (tupleDataName 2)
+#if MIN_VERSION_template_haskell(2,18,0)
+               []
+#endif
+               [mk_tuple_pat qv, rest_pat], zipped)
 
 -- helper function for dsParComp
 mk_tuple_stmt :: OSet Name -> Stmt
@@ -553,7 +569,11 @@ dsPat (VarP n) = return $ DVarP n
 dsPat (TupP pats) = DConP (tupleDataName (length pats)) <$> mapM dsPat pats
 dsPat (UnboxedTupP pats) = DConP (unboxedTupleDataName (length pats)) <$>
                            mapM dsPat pats
-dsPat (ConP name pats) = DConP name <$> mapM dsPat pats
+dsPat (ConP name
+#if MIN_VERSION_template_haskell(2,18,0)
+            _
+#endif
+            pats) = DConP name <$> mapM dsPat pats
 dsPat (InfixP p1 name p2) = DConP name <$> mapM dsPat [p1, p2]
 dsPat (UInfixP _ _ _) =
   fail "Cannot desugar unresolved infix operators."
@@ -779,7 +799,7 @@ dsDec (KiSigD n ki) = (:[]) <$> (DKiSigD n <$> dsType ki)
 
 -- | Desugar a 'DataD' or 'NewtypeD'.
 dsDataDec :: DsMonad q
-          => NewOrData -> Cxt -> Name -> [TyVarBndr]
+          => NewOrData -> Cxt -> Name -> [TyVarBndrUnit]
           -> Maybe Kind -> [Con] -> [DerivingClause] -> q [DDec]
 dsDataDec nd cxt n tvbs mk cons derivings = do
   tvbs' <- mapM dsTvb tvbs
@@ -796,7 +816,7 @@ dsDataDec nd cxt n tvbs mk cons derivings = do
 
 -- | Desugar a 'DataInstD' or a 'NewtypeInstD'.
 dsDataInstDec :: DsMonad q
-              => NewOrData -> Cxt -> Name -> Maybe [TyVarBndr] -> [TypeArg]
+              => NewOrData -> Cxt -> Name -> Maybe [TyVarBndrUnit] -> [TypeArg]
               -> Maybe Kind -> [Con] -> [DerivingClause] -> q [DDec]
 dsDataInstDec nd cxt n mtvbs tys mk cons derivings = do
   mtvbs' <- mapM (mapM dsTvb) mtvbs
@@ -1134,6 +1154,10 @@ dsClauses n clauses@(Clause outer_pats _ _ : _) = do
 
 -- | Desugar a type
 dsType :: DsMonad q => Type -> q DType
+#if __GLASGOW_HASKELL__ >= 811
+dsType (MulArrowT `AppT` _) = return DArrowT
+dsType MulArrowT = fail "Cannot desugar exotic uses of linear types."
+#endif
 dsType (ForallT tvbs preds ty) =
   mkDForallConstrainedT ForallInvis <$> mapM dsTvb tvbs <*> dsCxt preds <*> dsType ty
 dsType (AppT t1 t2) = DAppT <$> dsType t1 <*> dsType t2
@@ -1178,9 +1202,8 @@ dsType (ForallVisT tvbs ty) = DForallT ForallVis <$> mapM dsTvb tvbs <*> dsType
 #endif
 
 -- | Desugar a @TyVarBndr@
-dsTvb :: DsMonad q => TyVarBndr -> q DTyVarBndr
-dsTvb (PlainTV n) = return $ DPlainTV n
-dsTvb (KindedTV n k) = DKindedTV n <$> dsType k
+dsTvb :: DsMonad q => TyVarBndr_ spec -> q DTyVarBndr
+dsTvb = elimTV (return . DPlainTV) (\n k -> DKindedTV n <$> dsType k)
 
 -- | Desugar a @Cxt@
 dsCxt :: DsMonad q => Cxt -> q DCxt
@@ -1294,7 +1317,7 @@ dsPred t@(ForallVisT {}) =
 #endif
 
 -- | Desugar a quantified constraint.
-dsForallPred :: DsMonad q => [TyVarBndr] -> Cxt -> Pred -> q DCxt
+dsForallPred :: DsMonad q => [TyVarBndrSpec] -> Cxt -> Pred -> q DCxt
 dsForallPred tvbs cxt p = do
   ps' <- dsPred p
   case ps' of
@@ -1375,7 +1398,11 @@ mkTupleDPat pats = DConP (tupleDataName (length pats)) pats
 -- | Make a tuple 'Pat' from a list of 'Pat's. Avoids using a 1-tuple.
 mkTuplePat :: [Pat] -> Pat
 mkTuplePat [pat] = pat
-mkTuplePat pats = ConP (tupleDataName (length pats)) pats
+mkTuplePat pats = ConP (tupleDataName (length pats))
+#if MIN_VERSION_template_haskell(2,18,0)
+                       []
+#endif
+                       pats
 
 -- | Is this pattern guaranteed to match?
 isUniversalPattern :: DsMonad q => DPat -> q Bool
diff --git a/Language/Haskell/TH/Desugar/Reify.hs b/Language/Haskell/TH/Desugar/Reify.hs
index 7b35177..fa9f1d5 100644
--- a/Language/Haskell/TH/Desugar/Reify.hs
+++ b/Language/Haskell/TH/Desugar/Reify.hs
@@ -44,7 +44,7 @@ import qualified Data.Foldable as F
 import Data.Foldable (foldMap)
 #endif
 import Data.Function (on)
-import Data.List
+import Data.List (deleteFirstsBy, find)
 import qualified Data.Map as Map
 import Data.Map (Map)
 import Data.Maybe
@@ -53,6 +53,7 @@ import Data.Set (Set)
 
 import Language.Haskell.TH.Datatype
 import Language.Haskell.TH.Instances ()
+import Language.Haskell.TH.Lib hiding (cxt)
 import Language.Haskell.TH.Syntax hiding ( lift )
 
 import Language.Haskell.TH.Desugar.Util
@@ -99,7 +100,7 @@ reifyFail name =
 getDataD :: DsMonad q
          => String       -- ^ Print this out on failure
          -> Name         -- ^ Name of the datatype (@data@ or @newtype@) of interest
-         -> q ([TyVarBndr], [Con])
+         -> q ([TyVarBndrUnit], [Con])
 getDataD err name = do
   info <- reifyWithLocals name
   dec <- case info of
@@ -139,16 +140,16 @@ getDataD err name = do
 -- are fresh type variable names.
 --
 -- This expands kind synonyms if necessary.
-mkExtraKindBinders :: forall q. Quasi q => Kind -> q [TyVarBndr]
+mkExtraKindBinders :: forall q. Quasi q => Kind -> q [TyVarBndrUnit]
 mkExtraKindBinders k = do
   k' <- runQ $ resolveTypeSynonyms k
   let (fun_args, _) = unravelType k'
       vis_fun_args  = filterVisFunArgs fun_args
   mapM mk_tvb vis_fun_args
   where
-    mk_tvb :: VisFunArg -> q TyVarBndr
+    mk_tvb :: VisFunArg -> q TyVarBndrUnit
     mk_tvb (VisFADep tvb) = return tvb
-    mk_tvb (VisFAAnon ki) = KindedTV <$> qNewName "a" <*> return ki
+    mk_tvb (VisFAAnon ki) = kindedTV <$> qNewName "a" <*> return ki
 
 -- | From the name of a data constructor, retrive the datatype definition it
 -- is a part of.
@@ -380,15 +381,15 @@ maybeReifyCon n _decs ty_name ty_args cons
 #endif
          )
   where
-    extract_rec_sel_info :: RecSelInfo -> ([TyVarBndr], Type, Type)
+    extract_rec_sel_info :: RecSelInfo -> ([TyVarBndrSpec], Type, Type)
       -- Returns ( Selector type variable binders
       --         , Record field type
       --         , constructor result type )
     extract_rec_sel_info rec_sel_info =
       case rec_sel_info of
-        RecSelH98 sel_ty -> (h98_tvbs, sel_ty, h98_res_ty)
+        RecSelH98 sel_ty -> (changeSpecs SpecifiedSpec h98_tvbs, sel_ty, h98_res_ty)
         RecSelGADT sel_ty con_res_ty ->
-          ( freeVariablesWellScoped [con_res_ty, sel_ty]
+          ( changeSpecs SpecifiedSpec $ freeVariablesWellScoped [con_res_ty, sel_ty]
           , sel_ty, con_res_ty)
 
     h98_tvbs   = freeVariablesWellScoped $ map probablyWrongUnTypeArg ty_args
@@ -416,7 +417,7 @@ This is contrast to GHC's own reification, which will produce `D a`
 -}
 
 -- Reverse-engineer the type of a data constructor.
-con_to_type :: [TyVarBndr] -- The type variables bound by a data type head.
+con_to_type :: [TyVarBndrUnit] -- The type variables bound by a data type head.
                            -- Only used for Haskell98-style constructors.
             -> Type        -- The constructor result type.
                            -- Only used for Haskell98-style constructors.
@@ -424,7 +425,7 @@ con_to_type :: [TyVarBndr] -- The type variables bound by a data type head.
 con_to_type h98_tvbs h98_result_ty con =
   case go con of
     (is_gadt, ty) | is_gadt   -> ty
-                  | otherwise -> maybeForallT h98_tvbs [] ty
+                  | otherwise -> maybeForallT (changeSpecs SpecifiedSpec h98_tvbs) [] ty
   where
     go :: Con -> (Bool, Type) -- The Bool is True when dealing with a GADT
     go (NormalC _ stys)       = (False, mkArrows (map snd    stys)  h98_result_ty)
@@ -520,14 +521,14 @@ findInstances n = map stripInstanceDec . concatMap match_instance
 #if __GLASGOW_HASKELL__ >= 807
     -- See Note [Rejigging reified type family equations variable binders]
     -- for why this is necessary.
-    rejig_tvbs :: [Type] -> Maybe [TyVarBndr]
+    rejig_tvbs :: [Type] -> Maybe [TyVarBndrUnit]
     rejig_tvbs ts =
       let tvbs = freeVariablesWellScoped ts
       in if null tvbs
          then Nothing
          else Just tvbs
 
-    rejig_data_inst_tvbs :: Cxt -> Type -> Maybe Kind -> Maybe [TyVarBndr]
+    rejig_data_inst_tvbs :: Cxt -> Type -> Maybe Kind -> Maybe [TyVarBndrUnit]
     rejig_data_inst_tvbs cxt lhs mk =
       rejig_tvbs $ cxt ++ [lhs] ++ maybeToList mk
 #endif
@@ -638,7 +639,7 @@ quantifyClassDecMethods dec = dec
 -- appear in the reified type, so `True` is appropriate.
 quantifyClassMethodType
   :: Name        -- ^ The class name.
-  -> [TyVarBndr] -- ^ The class's type variable binders.
+  -> [TyVarBndrUnit] -- ^ The class's type variable binders.
   -> Bool        -- ^ If 'True', prepend a class predicate.
   -> Type        -- ^ The method type.
   -> Type
@@ -647,7 +648,7 @@ quantifyClassMethodType cls_name cls_tvbs prepend meth_ty =
   where
     add_cls_cxt :: Type -> Type
     add_cls_cxt
-      | prepend   = ForallT all_cls_tvbs cls_cxt
+      | prepend   = ForallT (changeSpecs SpecifiedSpec all_cls_tvbs) cls_cxt
       | otherwise = id
 
     cls_cxt :: Cxt
@@ -666,12 +667,13 @@ quantifyClassMethodType cls_name cls_tvbs prepend meth_ty =
       | otherwise
       = ForallT meth_tvbs [] meth_ty
 
-    meth_tvbs :: [TyVarBndr]
-    meth_tvbs = deleteFirstsBy ((==) `on` tvName)
+    meth_tvbs :: [TyVarBndrSpec]
+    meth_tvbs = changeSpecs SpecifiedSpec $
+                deleteFirstsBy ((==) `on` tvName)
                   (freeVariablesWellScoped [meth_ty]) all_cls_tvbs
 
     -- Explicitly quantify any kind variables bound by the class, if any.
-    all_cls_tvbs :: [TyVarBndr]
+    all_cls_tvbs :: [TyVarBndrUnit]
     all_cls_tvbs = freeVariablesWellScoped $ map tvbToTypeWithSig cls_tvbs
 
 stripInstanceDec :: Dec -> Dec
@@ -686,7 +688,7 @@ mkArrows :: [Type] -> Type -> Type
 mkArrows []     res_ty = res_ty
 mkArrows (t:ts) res_ty = AppT (AppT ArrowT t) $ mkArrows ts res_ty
 
-maybeForallT :: [TyVarBndr] -> Cxt -> Type -> Type
+maybeForallT :: [TyVarBndrSpec] -> Cxt -> Type -> Type
 maybeForallT tvbs cxt ty
   | null tvbs && null cxt        = ty
   | ForallT tvbs2 cxt2 ty2 <- ty = ForallT (tvbs ++ tvbs2) (cxt ++ cxt2) ty2
@@ -969,17 +971,17 @@ find_assoc_type_kind n cls_tvb_kind_map sub_dec =
 #endif
     _ -> Nothing
   where
-    ascribe_tf_tvb_kind :: TyVarBndr -> TyVarBndr
+    ascribe_tf_tvb_kind :: TyVarBndrUnit -> TyVarBndrUnit
     ascribe_tf_tvb_kind tvb =
-      case tvb of
-        KindedTV{}  -> tvb
-        PlainTV tvn -> KindedTV tvn $ fromMaybe StarT $ Map.lookup tvn cls_tvb_kind_map
+      elimTV (\tvn -> kindedTV tvn $ fromMaybe StarT $ Map.lookup tvn cls_tvb_kind_map)
+             (\_ _ -> tvb)
+             tvb
 
 -- Data types have CUSKs when:
 --
 -- 1. All of their type variables have explicit kinds.
 -- 2. All kind variables in the result kind are explicitly quantified.
-datatype_kind :: [TyVarBndr] -> Maybe Kind -> Maybe Kind
+datatype_kind :: [TyVarBndrUnit] -> Maybe Kind -> Maybe Kind
 datatype_kind tvbs m_ki =
   whenAlt (all tvb_is_kinded tvbs && ki_fvs_are_bound) $
   build_kind tvbs (default_res_ki m_ki)
@@ -991,14 +993,14 @@ datatype_kind tvbs m_ki =
       in ki_fvs `Set.isSubsetOf` tvb_vars
 
 -- Classes have CUSKs when all of their type variables have explicit kinds.
-class_kind :: [TyVarBndr] -> Maybe Kind
+class_kind :: [TyVarBndrUnit] -> Maybe Kind
 class_kind tvbs = whenAlt (all tvb_is_kinded tvbs) $
                   build_kind tvbs ConstraintT
 
 -- Open type families and data families always have CUSKs. Type variables
 -- without explicit kinds default to Type, as does the return kind if it
 -- is not specified.
-open_ty_fam_kind :: [TyVarBndr] -> Maybe Kind -> Maybe Kind
+open_ty_fam_kind :: [TyVarBndrUnit] -> Maybe Kind -> Maybe Kind
 open_ty_fam_kind tvbs m_ki =
   build_kind (map default_tvb tvbs) (default_res_ki m_ki)
 
@@ -1006,7 +1008,7 @@ open_ty_fam_kind tvbs m_ki =
 --
 -- 1. All of their type variables have explicit kinds.
 -- 2. An explicit return kind is supplied.
-closed_ty_fam_kind :: [TyVarBndr] -> Maybe Kind -> Maybe Kind
+closed_ty_fam_kind :: [TyVarBndrUnit] -> Maybe Kind -> Maybe Kind
 closed_ty_fam_kind tvbs m_ki =
   case m_ki of
     Just ki -> whenAlt (all tvb_is_kinded tvbs) $
@@ -1017,7 +1019,7 @@ closed_ty_fam_kind tvbs m_ki =
 --
 -- 1. All of their type variables have explicit kinds.
 -- 2. The right-hand-side type is annotated with an explicit kind.
-ty_syn_kind :: [TyVarBndr] -> Type -> Maybe Kind
+ty_syn_kind :: [TyVarBndrUnit] -> Type -> Maybe Kind
 ty_syn_kind tvbs rhs =
   case rhs of
     SigT _ ki -> whenAlt (all tvb_is_kinded tvbs) $
@@ -1029,15 +1031,14 @@ ty_syn_kind tvbs rhs =
 -- this function is `Maybe Kind` because there are situations where even
 -- this amount of information is not sufficient to determine the full kind.
 -- See Note [The limitations of standalone kind signatures].
-build_kind :: [TyVarBndr] -> Kind -> Maybe Kind
+build_kind :: [TyVarBndrUnit] -> Kind -> Maybe Kind
 build_kind arg_kinds res_kind =
   fmap quantifyType $ fst $
   foldr go (Just res_kind, Set.fromList (freeVariables res_kind)) arg_kinds
   where
-    go :: TyVarBndr -> (Maybe Kind, Set Name) -> (Maybe Kind, Set Name)
-    go tvb (res, res_fvs) =
-      case tvb of
-        PlainTV n
+    go :: TyVarBndrUnit -> (Maybe Kind, Set Name) -> (Maybe Kind, Set Name)
+    go tvb (res, res_fvs) = elimTV
+      (\n
           -> ( if n `Set.member` res_fvs
                then forall_vis tvb res
                else Nothing -- We have a type variable binder without an
@@ -1045,15 +1046,16 @@ build_kind arg_kinds res_kind =
                             -- we cannot build a kind from it. This is the
                             -- only case where we return Nothing.
              , res_fvs
-             )
-        KindedTV n k
+             ))
+      (\n k
           -> ( if n `Set.member` res_fvs
                then forall_vis tvb res
                else fmap (ArrowT `AppT` k `AppT`) res
              , Set.fromList (freeVariables k) `Set.union` res_fvs
-             )
+             ))
+      tvb
 
-    forall_vis :: TyVarBndr -> Maybe Kind -> Maybe Kind
+    forall_vis :: TyVarBndrUnit -> Maybe Kind -> Maybe Kind
 #if __GLASGOW_HASKELL__ >= 809
     forall_vis tvb m_ki = fmap (ForallVisT [tvb]) m_ki
       -- One downside of this approach is that we generate kinds like this:
@@ -1069,20 +1071,18 @@ build_kind arg_kinds res_kind =
     forall_vis _   _    = Nothing
 #endif
 
-tvb_is_kinded :: TyVarBndr -> Bool
+tvb_is_kinded :: TyVarBndr_ spec -> Bool
 tvb_is_kinded = isJust . tvb_kind_maybe
 
-tvb_kind_maybe :: TyVarBndr -> Maybe Kind
-tvb_kind_maybe PlainTV{}      = Nothing
-tvb_kind_maybe (KindedTV _ k) = Just k
+tvb_kind_maybe :: TyVarBndr_ spec -> Maybe Kind
+tvb_kind_maybe = elimTV (\_ -> Nothing) (\_ -> Just)
 
 vis_arg_kind_maybe :: VisFunArg -> Maybe Kind
 vis_arg_kind_maybe (VisFADep tvb) = tvb_kind_maybe tvb
 vis_arg_kind_maybe (VisFAAnon k)  = Just k
 
-default_tvb :: TyVarBndr -> TyVarBndr
-default_tvb (PlainTV n)    = KindedTV n StarT
-default_tvb tvb@KindedTV{} = tvb
+default_tvb :: TyVarBndrUnit -> TyVarBndrUnit
+default_tvb tvb = elimTV (\n -> kindedTV n StarT) (\_ _ -> tvb) tvb
 
 default_res_ki :: Maybe Kind -> Kind
 default_res_ki = fromMaybe StarT
diff --git a/Language/Haskell/TH/Desugar/Sweeten.hs b/Language/Haskell/TH/Desugar/Sweeten.hs
index 4a18d57..a1f6f7c 100644
--- a/Language/Haskell/TH/Desugar/Sweeten.hs
+++ b/Language/Haskell/TH/Desugar/Sweeten.hs
@@ -28,7 +28,7 @@ module Language.Haskell.TH.Desugar.Sweeten (
   letDecToTH, typeToTH,
 
   conToTH, foreignToTH, pragmaToTH, ruleBndrToTH,
-  clauseToTH, tvbToTH, cxtToTH, predToTH, derivClauseToTH,
+  clauseToTH, tvbToTHUnit, cxtToTH, predToTH, derivClauseToTH,
 #if __GLASGOW_HASKELL__ >= 801
   patSynDirToTH,
 #endif
@@ -75,7 +75,11 @@ matchToTH (DMatch pat exp) = Match (patToTH pat) (NormalB (expToTH exp)) []
 patToTH :: DPat -> Pat
 patToTH (DLitP lit)    = LitP lit
 patToTH (DVarP n)      = VarP n
-patToTH (DConP n pats) = ConP n (map patToTH pats)
+patToTH (DConP n pats) = ConP n
+#if MIN_VERSION_template_haskell(2,18,0)
+                              []
+#endif
+                              (map patToTH pats)
 patToTH (DTildeP pat)  = TildeP (patToTH pat)
 patToTH (DBangP pat)   = BangP (patToTH pat)
 patToTH (DSigP pat ty) = SigP (patToTH pat) (typeToTH ty)
@@ -90,15 +94,15 @@ decToTH :: DDec -> [Dec]
 decToTH (DLetDec d) = maybeToList (letDecToTH d)
 decToTH (DDataD Data cxt n tvbs _mk cons derivings) =
 #if __GLASGOW_HASKELL__ > 710
-  [DataD (cxtToTH cxt) n (map tvbToTH tvbs) (fmap typeToTH _mk) (map conToTH cons)
+  [DataD (cxtToTH cxt) n (map tvbToTHUnit tvbs) (fmap typeToTH _mk) (map conToTH cons)
          (concatMap derivClauseToTH derivings)]
 #else
-  [DataD (cxtToTH cxt) n (map tvbToTH tvbs) (map conToTH cons)
+  [DataD (cxtToTH cxt) n (map tvbToTHUnit tvbs) (map conToTH cons)
          (map derivingToTH derivings)]
 #endif
 decToTH (DDataD Newtype cxt n tvbs _mk [con] derivings) =
 #if __GLASGOW_HASKELL__ > 710
-  [NewtypeD (cxtToTH cxt) n (map tvbToTH tvbs) (fmap typeToTH _mk) (conToTH con)
+  [NewtypeD (cxtToTH cxt) n (map tvbToTHUnit tvbs) (fmap typeToTH _mk) (conToTH con)
             (concatMap derivClauseToTH derivings)]
 #else
   [NewtypeD (cxtToTH cxt) n (map tvbToTH tvbs) (conToTH con)
@@ -106,9 +110,9 @@ decToTH (DDataD Newtype cxt n tvbs _mk [con] derivings) =
 #endif
 decToTH (DDataD Newtype _cxt _n _tvbs _mk _cons _derivings) =
   error "Newtype declaration without exactly 1 constructor."
-decToTH (DTySynD n tvbs ty) = [TySynD n (map tvbToTH tvbs) (typeToTH ty)]
+decToTH (DTySynD n tvbs ty) = [TySynD n (map tvbToTHUnit tvbs) (typeToTH ty)]
 decToTH (DClassD cxt n tvbs fds decs) =
-  [ClassD (cxtToTH cxt) n (map tvbToTH tvbs) fds (decsToTH decs)]
+  [ClassD (cxtToTH cxt) n (map tvbToTHUnit tvbs) fds (decsToTH decs)]
 decToTH (DInstanceD over mtvbs _cxt _ty decs) =
   [instanceDToTH over cxt' ty' decs]
   where
@@ -125,16 +129,16 @@ decToTH (DInstanceD over mtvbs _cxt _ty decs) =
 decToTH (DForeignD f) = [ForeignD (foreignToTH f)]
 #if __GLASGOW_HASKELL__ > 710
 decToTH (DOpenTypeFamilyD (DTypeFamilyHead n tvbs frs ann)) =
-  [OpenTypeFamilyD (TypeFamilyHead n (map tvbToTH tvbs) (frsToTH frs) ann)]
+  [OpenTypeFamilyD (TypeFamilyHead n (map tvbToTHUnit tvbs) (frsToTH frs) ann)]
 #else
 decToTH (DOpenTypeFamilyD (DTypeFamilyHead n tvbs frs _ann)) =
-  [FamilyD TypeFam n (map tvbToTH tvbs) (frsToTH frs)]
+  [FamilyD TypeFam n (map tvbToTHUnit tvbs) (frsToTH frs)]
 #endif
 decToTH (DDataFamilyD n tvbs mk) =
 #if __GLASGOW_HASKELL__ > 710
-  [DataFamilyD n (map tvbToTH tvbs) (fmap typeToTH mk)]
+  [DataFamilyD n (map tvbToTHUnit tvbs) (fmap typeToTH mk)]
 #else
-  [FamilyD DataFam n (map tvbToTH tvbs) (fmap typeToTH mk)]
+  [FamilyD DataFam n (map tvbToTHUnit tvbs) (fmap typeToTH mk)]
 #endif
 decToTH (DDataInstD nd cxt mtvbs lhs mk cons derivings) =
   let ndc = case (nd, cons) of
@@ -151,12 +155,12 @@ decToTH (DTySynInstD eqn) =
 #endif
 #if __GLASGOW_HASKELL__ > 710
 decToTH (DClosedTypeFamilyD (DTypeFamilyHead n tvbs frs ann) eqns) =
-  [ClosedTypeFamilyD (TypeFamilyHead n (map tvbToTH tvbs) (frsToTH frs) ann)
+  [ClosedTypeFamilyD (TypeFamilyHead n (map tvbToTHUnit tvbs) (frsToTH frs) ann)
                      (map (snd . tySynEqnToTH) eqns)
   ]
 #else
 decToTH (DClosedTypeFamilyD (DTypeFamilyHead n tvbs frs _ann) eqns) =
-  [ClosedTypeFamilyD n (map tvbToTH tvbs) (frsToTH frs) (map (snd . tySynEqnToTH) eqns)]
+  [ClosedTypeFamilyD n (map tvbToTHUnit tvbs) (frsToTH frs) (map (snd . tySynEqnToTH) eqns)]
 #endif
 decToTH (DRoleAnnotD n roles) = [RoleAnnotD n roles]
 decToTH (DStandaloneDerivD mds mtvbs _cxt _ty) =
@@ -210,11 +214,11 @@ dataInstDecToTH ndc cxt _mtvbs lhs _mk derivings =
   case ndc of
     DNewtypeCon con ->
 #if __GLASGOW_HASKELL__ >= 807
-      [NewtypeInstD (cxtToTH cxt) (fmap (fmap tvbToTH) _mtvbs) (typeToTH lhs)
+      [NewtypeInstD (cxtToTH cxt) (fmap (fmap tvbToTHUnit) _mtvbs) (typeToTH lhs)
                     (fmap typeToTH _mk) (conToTH con)
                     (concatMap derivClauseToTH derivings)]
 #elif __GLASGOW_HASKELL__ > 710
-      [NewtypeInstD (cxtToTH cxt) _n _lhs_args (fmap typeToTH _mk) (conToTH con)
+      [NewtypeInstD (cxtToTH cxt) _n _lhs_args (fmap typeToTHUnit _mk) (conToTH con)
                     (concatMap derivClauseToTH derivings)]
 #else
       [NewtypeInstD (cxtToTH cxt) _n _lhs_args (conToTH con)
@@ -223,11 +227,11 @@ dataInstDecToTH ndc cxt _mtvbs lhs _mk derivings =
 
     DDataCons cons ->
 #if __GLASGOW_HASKELL__ >= 807
-      [DataInstD (cxtToTH cxt) (fmap (fmap tvbToTH) _mtvbs) (typeToTH lhs)
+      [DataInstD (cxtToTH cxt) (fmap (fmap tvbToTHUnit) _mtvbs) (typeToTH lhs)
                  (fmap typeToTH _mk) (map conToTH cons)
                  (concatMap derivClauseToTH derivings)]
 #elif __GLASGOW_HASKELL__ > 710
-      [DataInstD (cxtToTH cxt) _n _lhs_args (fmap typeToTH _mk) (map conToTH cons)
+      [DataInstD (cxtToTH cxt) _n _lhs_args (fmap typeToTHUnit _mk) (map conToTH cons)
                  (concatMap derivClauseToTH derivings)]
 #else
       [DataInstD (cxtToTH cxt) _n _lhs_args (map conToTH cons)
@@ -244,7 +248,7 @@ dataInstDecToTH ndc cxt _mtvbs lhs _mk derivings =
 frsToTH :: DFamilyResultSig -> FamilyResultSig
 frsToTH DNoSig          = NoSig
 frsToTH (DKindSig k)    = KindSig (typeToTH k)
-frsToTH (DTyVarSig tvb) = TyVarSig (tvbToTH tvb)
+frsToTH (DTyVarSig tvb) = TyVarSig (tvbToTHUnit tvb)
 #else
 frsToTH :: DFamilyResultSig -> Maybe Kind
 frsToTH DNoSig                      = Nothing
@@ -291,7 +295,7 @@ conToTH (DCon [] [] n (DRecC vstys) _) =
 -- perfectly OK to put all of the quantified type variables
 -- (both universal and existential) in a ForallC.
 conToTH (DCon tvbs cxt n fields rty) =
-  ForallC (map tvbToTH tvbs) (cxtToTH cxt) (conToTH $ DCon [] [] n fields rty)
+  ForallC (map tvbToTHSpec tvbs) (cxtToTH cxt) (conToTH $ DCon [] [] n fields rty)
 #else
 -- On GHCs earlier than 8.0, we must be careful, since the only time ForallC is
 -- used is when there are either:
@@ -370,7 +374,7 @@ pragmaToTH (DSpecialiseP n ty m_inl phases) =
 pragmaToTH (DSpecialiseInstP ty) = Just $ SpecialiseInstP (typeToTH ty)
 #if __GLASGOW_HASKELL__ >= 807
 pragmaToTH (DRuleP str mtvbs rbs lhs rhs phases) =
-  Just $ RuleP str (fmap (fmap tvbToTH) mtvbs) (map ruleBndrToTH rbs)
+  Just $ RuleP str (fmap (fmap tvbToTHUnit) mtvbs) (map ruleBndrToTH rbs)
                (expToTH lhs) (expToTH rhs) phases
 #else
 pragmaToTH (DRuleP str _ rbs lhs rhs phases) =
@@ -399,7 +403,7 @@ tySynEqnToTH :: DTySynEqn -> (Name, TySynEqn)
 tySynEqnToTH (DTySynEqn tvbs lhs rhs) =
   let lhs' = typeToTH lhs in
   case unfoldType lhs' of
-    (ConT n, _lhs_args) -> (n, TySynEqn (fmap (fmap tvbToTH) tvbs) lhs' (typeToTH rhs))
+    (ConT n, _lhs_args) -> (n, TySynEqn (fmap (fmap tvbToTHUnit) tvbs) lhs' (typeToTH rhs))
     (_, _) -> error $ "Illegal type instance LHS: " ++ pprint lhs'
 #else
 tySynEqnToTH :: DTySynEqn -> (Name, TySynEqn)
@@ -418,18 +422,17 @@ typeToTH :: DType -> Type
 -- so that we may collapse them into a single ForallT when sweetening.
 -- See Note [Desugaring and sweetening ForallT] in L.H.T.Desugar.Core.
 typeToTH (DForallT ForallInvis tvbs (DConstrainedT ctxt ty)) =
-  ForallT (map tvbToTH tvbs) (map predToTH ctxt) (typeToTH ty)
+  ForallT (map tvbToTHSpec tvbs) (map predToTH ctxt) (typeToTH ty)
 typeToTH (DForallT fvf tvbs ty) =
   case fvf of
-    ForallInvis -> ForallT tvbs' [] ty'
+    ForallInvis -> ForallT (map tvbToTHSpec tvbs) [] ty'
     ForallVis ->
 #if __GLASGOW_HASKELL__ >= 809
-      ForallVisT tvbs' ty'
+      ForallVisT (map tvbToTHUnit tvbs) ty'
 #else
       error "Visible dependent quantification supported only in GHC 8.10+"
 #endif
   where
-    tvbs' = map tvbToTH tvbs
     ty'   = typeToTH ty
 typeToTH (DConstrainedT cxt ty) = ForallT [] (map predToTH cxt) (typeToTH ty)
 typeToTH (DAppT t1 t2)          = AppT (typeToTH t1) (typeToTH t2)
@@ -451,9 +454,20 @@ typeToTH (DAppKindT t k)        = AppKindT (typeToTH t) (typeToTH k)
 typeToTH (DAppKindT t _)        = typeToTH t
 #endif
 
-tvbToTH :: DTyVarBndr -> TyVarBndr
-tvbToTH (DPlainTV n)           = PlainTV n
-tvbToTH (DKindedTV n k)        = KindedTV n (typeToTH k)
+tvbToTHSpec :: DTyVarBndr -> TyVarBndrSpec
+tvbToTHUnit :: DTyVarBndr -> TyVarBndrUnit
+#if MIN_VERSION_template_haskell(2,17,0)
+tvbToTHSpec (DPlainTV n)       = PlainTV n SpecifiedSpec
+tvbToTHSpec (DKindedTV n k)    = KindedTV n SpecifiedSpec (typeToTH k)
+
+tvbToTHUnit (DPlainTV n)       = PlainTV n ()
+tvbToTHUnit (DKindedTV n k)    = KindedTV n () (typeToTH k)
+#else
+tvbToTHSpec (DPlainTV n)       = PlainTV n
+tvbToTHSpec (DKindedTV n k)    = KindedTV n (typeToTH k)
+
+tvbToTHUnit = tvbToTHSpec
+#endif
 
 cxtToTH :: DCxt -> Cxt
 cxtToTH = map predToTH
@@ -530,13 +544,13 @@ predToTH DWildCardT  = error "Wildcards supported only in GHC 8.0+"
 -- so that we may collapse them into a single ForallT when sweetening.
 -- See Note [Desugaring and sweetening ForallT] in L.H.T.Desugar.Core.
 predToTH (DForallT ForallInvis tvbs (DConstrainedT ctxt p)) =
-  ForallT (map tvbToTH tvbs) (map predToTH ctxt) (predToTH p)
+  ForallT (map tvbToTHSpec tvbs) (map predToTH ctxt) (predToTH p)
 predToTH (DForallT fvf tvbs p) =
   case fvf of
     ForallInvis -> ForallT tvbs' [] p'
     ForallVis   -> error "Visible dependent quantifier spotted at head of a constraint"
   where
-    tvbs' = map tvbToTH tvbs
+    tvbs' = map tvbToTHSpec tvbs
     p'    = predToTH p
 predToTH (DConstrainedT cxt p) = ForallT [] (map predToTH cxt) (predToTH p)
 #else
diff --git a/Language/Haskell/TH/Desugar/Util.hs b/Language/Haskell/TH/Desugar/Util.hs
index dd2f621..d63ad9d 100644
--- a/Language/Haskell/TH/Desugar/Util.hs
+++ b/Language/Haskell/TH/Desugar/Util.hs
@@ -36,6 +36,7 @@ module Language.Haskell.TH.Desugar.Util (
 #if __GLASGOW_HASKELL__ >= 800
   , bindIP
 #endif
+  , TyVarBndr_, TyVarBndrSpec, TyVarBndrUnit, Specificity(..), elimTV, changeSpecs
   ) where
 
 import Prelude hiding (mapM, foldl, concatMap, any)
@@ -111,9 +112,8 @@ stripVarP_maybe (VarP name) = Just name
 stripVarP_maybe _           = Nothing
 
 -- | Extracts the name out of a @PlainTV@, or returns @Nothing@
-stripPlainTV_maybe :: TyVarBndr -> Maybe Name
-stripPlainTV_maybe (PlainTV n) = Just n
-stripPlainTV_maybe _           = Nothing
+stripPlainTV_maybe :: TyVarBndr_ spec -> Maybe Name
+stripPlainTV_maybe = elimTV Just (\_ _ -> Nothing)
 
 -- | Report that a certain TH construct is impossible
 impossible :: Fail.MonadFail q => String -> q a
@@ -121,18 +121,17 @@ impossible err = Fail.fail (err ++ "\n    This should not happen in Haskell.\n
 
 -- | Convert a 'TyVarBndr' into a 'Type', dropping the kind signature
 -- (if it has one).
-tvbToType :: TyVarBndr -> Type
+tvbToType :: TyVarBndr_ spec -> Type
 tvbToType = VarT . tvName
 
 -- | Convert a 'TyVarBndr' into a 'Type', preserving the kind signature
 -- (if it has one).
-tvbToTypeWithSig :: TyVarBndr -> Type
-tvbToTypeWithSig (PlainTV n)    = VarT n
-tvbToTypeWithSig (KindedTV n k) = SigT (VarT n) k
+tvbToTypeWithSig :: TyVarBndr_ spec -> Type
+tvbToTypeWithSig = elimTV VarT (\n k -> SigT (VarT n) k)
 
 -- | Convert a 'TyVarBndr' into a 'TypeArg' (specifically, a 'TANormal'),
 -- preserving the kind signature (if it has one).
-tvbToTANormalWithSig :: TyVarBndr -> TypeArg
+tvbToTANormalWithSig :: TyVarBndr_ spec -> TypeArg
 tvbToTANormalWithSig = TANormal . tvbToTypeWithSig
 
 -- | Do two names name the same thing?
@@ -217,7 +216,7 @@ data ForallVisFlag
 data FunArgs
   = FANil
     -- ^ No more arguments.
-  | FAForalls ForallVisFlag [TyVarBndr] FunArgs
+  | FAForalls ForallVisFlag [TyVarBndrSpec] FunArgs
     -- ^ A series of @forall@ed type variables followed by a dot (if
     --   'ForallInvis') or an arrow (if 'ForallVis'). For example,
     --   the type variables @a1 ... an@ in @forall a1 ... an. r@.
@@ -234,7 +233,7 @@ data FunArgs
 -- arguments (e.g., the @c@ in @c => r@), which are instantiated without
 -- the need for explicit user input.
 data VisFunArg
-  = VisFADep TyVarBndr
+  = VisFADep TyVarBndrUnit
     -- ^ A visible @forall@ (e.g., @forall a -> a@).
   | VisFAAnon Type
     -- ^ An anonymous argument followed by an arrow (e.g., @a -> r@).
@@ -245,7 +244,7 @@ filterVisFunArgs :: FunArgs -> [VisFunArg]
 filterVisFunArgs FANil = []
 filterVisFunArgs (FAForalls fvf tvbs args) =
   case fvf of
-    ForallVis   -> map VisFADep tvbs ++ args'
+    ForallVis   -> map VisFADep (changeSpecs () tvbs) ++ args'
     ForallInvis -> args'
   where
     args' = filterVisFunArgs args
@@ -265,7 +264,7 @@ ravelType (FAForalls ForallInvis tvbs (FACxt p args)) res =
 ravelType (FAForalls ForallInvis  tvbs  args)  res = ForallT tvbs [] (ravelType args res)
 ravelType (FAForalls ForallVis   _tvbs _args) _res =
 #if __GLASGOW_HASKELL__ >= 809
-      ForallVisT _tvbs (ravelType _args _res)
+      ForallVisT (changeSpecs () _tvbs) (ravelType _args _res)
 #else
       error "Visible dependent quantification supported only on GHC 8.10+"
 #endif
@@ -284,7 +283,7 @@ unravelType (AppT (AppT ArrowT t1) t2) =
 #if __GLASGOW_HASKELL__ >= 809
 unravelType (ForallVisT tvbs ty) =
   let (args, res) = unravelType ty in
-  (FAForalls ForallVis tvbs args, res)
+  (FAForalls ForallVis (changeSpecs SpecifiedSpec tvbs) args, res)
 #endif
 unravelType t = (FANil, t)
 
@@ -414,7 +413,11 @@ extractBoundNamesPat (LitP _)              = OS.empty
 extractBoundNamesPat (VarP name)           = OS.singleton name
 extractBoundNamesPat (TupP pats)           = foldMap extractBoundNamesPat pats
 extractBoundNamesPat (UnboxedTupP pats)    = foldMap extractBoundNamesPat pats
-extractBoundNamesPat (ConP _ pats)         = foldMap extractBoundNamesPat pats
+extractBoundNamesPat (ConP _
+#if MIN_VERSION_template_haskell(2,18,0)
+                           _
+#endif
+                             pats)         = foldMap extractBoundNamesPat pats
 extractBoundNamesPat (InfixP p1 _ p2)      = extractBoundNamesPat p1 `OS.union`
                                              extractBoundNamesPat p2
 extractBoundNamesPat (UInfixP p1 _ p2)     = extractBoundNamesPat p1 `OS.union`
@@ -566,3 +569,31 @@ uniStarKindName = ''(Kind.★)
 uniStarKindName = starKindName
 #endif
 #endif
+
+#if MIN_VERSION_template_haskell(2,17,0)
+type TyVarBndr_ spec = TyVarBndr spec
+#else
+type TyVarBndr_ spec = TyVarBndr
+type TyVarBndrSpec   = TyVarBndr
+type TyVarBndrUnit   = TyVarBndr
+
+data Specificity
+  = SpecifiedSpec
+  | InferredSpec
+#endif
+
+elimTV :: (Name -> r) -> (Name -> Kind -> r) -> TyVarBndr_ spec -> r
+#if MIN_VERSION_template_haskell(2,17,0)
+elimTV ptv _ktv (PlainTV n _)    = ptv n
+elimTV _ptv ktv (KindedTV n _ k) = ktv n k
+#else
+elimTV ptv _ktv (PlainTV n)    = ptv n
+elimTV _ptv ktv (KindedTV n k) = ktv n k
+#endif
+
+changeSpecs :: newSpec -> [TyVarBndr_ oldSpec] -> [TyVarBndr_ newSpec]
+#if MIN_VERSION_template_haskell(2,17,0)
+changeSpecs newSpec = map (newSpec <$)
+#else
+changeSpecs _ = id
+#endif
